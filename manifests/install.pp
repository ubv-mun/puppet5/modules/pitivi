# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include pitivi::install
class pitivi::install {
  package { $pitivi::package_name:
    ensure => $pitivi::package_ensure,
  }
}
